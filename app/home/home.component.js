"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app = require("tns-core-modules/application/application");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var segmented_bar_1 = require("tns-core-modules/ui/segmented-bar/segmented-bar");
var element_registry_1 = require("nativescript-angular/element-registry");
var nativescript_cardview_1 = require("nativescript-cardview");
element_registry_1.registerElement('CardView', function () { return nativescript_cardview_1.CardView; });
var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, routerExtensions) {
        this.router = router;
        this.routerExtensions = routerExtensions;
        this.images = [];
        this.img = false;
        this.prop = "Item 1";
        this.rows = [];
        this.cols = [];
        this.datadisplay = [];
        this.rowsCount = 0;
        this.finaldata = [];
        this.data = [
            { "name": "Suit", "Product_name": "res://mann", "Product_code": "1" },
            { "name": "Jackets", "Product_name": "res://img", "Product_code": "1" },
            { "name": "Black tshirt", "Product_name": "res://imgg", "Product_code": "1" },
            { "name": "Man Hoodie", "Product_name": "res://imgee", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://imgee", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://imgee", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
            { "name": "", "Product_name": "res://img", "Product_code": "1" },
        ];
        this.arryrows = "*,";
        this.images = [
            { title: 'Spoorthy', url: 'res://sp' },
            { title: 'Image 2', url: 'res://spc' },
            { title: 'Image 3', url: 'res://spcm' },
        ];
        this.myItems = [];
        for (var i = 1; i < 6; i++) {
            var item = new segmented_bar_1.SegmentedBarItem();
            item.title = "Tab " + i;
            this.myItems.push(item);
        }
    }
    HomeComponent.prototype.ngOnInit = function () {
        // Init your component properties here.
        this.setScreendata();
    };
    HomeComponent.prototype.setScreendata = function () {
        var _this = this;
        var row = 0;
        var column = 0;
        var count = 0;
        this.data.forEach(function (element) {
            _this.datadisplay.push({
                rowcount: row,
                columncount: column,
                name: element.name,
                imgurl: element.Product_name,
                ProductCode: element.Product_code
            });
            if (count == 1) {
                row = row + 1;
                count = 0;
                _this.arryrows += "*,";
            }
            else {
                count = 1;
            }
            if (column == 0) {
                column = 1;
            }
            else {
                column = 0;
            }
        });
        this.arryrows = this.arryrows.substring(0, this.arryrows.length - 1);
        // console.log("DATA-Display", this.datadisplay);
        // console.log("this.arryrows", this.arryrows)
        // this.isAuthenticating = false;
    };
    HomeComponent.prototype.onNavItemTap = function (navItemRoute) {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });
    };
    HomeComponent.prototype.onDrawerButtonTap = function () {
        var sideDrawer = app.getRootView();
        sideDrawer.showDrawer();
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ["./home.component.css"],
        }),
        __metadata("design:paramtypes", [router_2.Router, router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw4REFBZ0U7QUFHaEUsc0RBQStEO0FBQy9ELDBDQUF3RDtBQUN4RCxpRkFBaUc7QUFDakcsMEVBQXdFO0FBQ3hFLCtEQUFpRDtBQUNqRCxrQ0FBZSxDQUFDLFVBQVUsRUFBRSxjQUFNLE9BQUEsZ0NBQVEsRUFBUixDQUFRLENBQUMsQ0FBQztBQVM1QztJQWtESSx1QkFBb0IsTUFBYyxFQUFVLGdCQUFrQztRQUExRCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQWpEOUUsV0FBTSxHQUFlLEVBQUUsQ0FBQztRQUN4QixRQUFHLEdBQVksS0FBSyxDQUFDO1FBRWQsU0FBSSxHQUFXLFFBQVEsQ0FBQztRQUN4QixTQUFJLEdBQUUsRUFBRSxDQUFDO1FBQ1QsU0FBSSxHQUFFLEVBQUUsQ0FBQztRQUNULGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxDQUFDLENBQUM7UUFDZCxjQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ2YsU0FBSSxHQUFHO1lBQ2QsRUFBQyxNQUFNLEVBQUMsTUFBTSxFQUFDLGNBQWMsRUFBQyxZQUFZLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUM5RCxFQUFDLE1BQU0sRUFBQyxTQUFTLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ2hFLEVBQUMsTUFBTSxFQUFDLGNBQWMsRUFBQyxjQUFjLEVBQUMsWUFBWSxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDdEUsRUFBQyxNQUFNLEVBQUMsWUFBWSxFQUFDLGNBQWMsRUFBQyxhQUFhLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUNyRSxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLGFBQWEsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQzNELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsYUFBYSxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDM0QsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztZQUN6RCxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsY0FBYyxFQUFDLFdBQVcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDO1lBQ3pELEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxjQUFjLEVBQUMsV0FBVyxFQUFDLGNBQWMsRUFBQyxHQUFHLEVBQUM7WUFDekQsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxXQUFXLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQztTQUN6QyxDQUFBO1FBQ1QsYUFBUSxHQUFHLElBQUksQ0FBQztRQUtuQixJQUFJLENBQUMsTUFBTSxHQUFHO1lBQ1YsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUU7WUFDdEMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxXQUFXLEVBQUU7WUFDdEMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUU7U0FDMUMsQ0FBQztRQUVGLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekIsSUFBTSxJQUFJLEdBQUcsSUFBSSxnQ0FBZ0IsRUFBRSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDO0lBQ0wsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO0lBQ3hCLENBQUM7SUFJRCxxQ0FBYSxHQUFiO1FBQUEsaUJBOEJHO1FBN0JDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUN2QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQkFDcEIsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsV0FBVyxFQUFFLE1BQU07Z0JBQ25CLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSTtnQkFDbEIsTUFBTSxFQUFDLE9BQU8sQ0FBQyxZQUFZO2dCQUMzQixXQUFXLEVBQUMsT0FBTyxDQUFDLFlBQVk7YUFFakMsQ0FBQyxDQUFBO1lBQ0YsRUFBRSxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ2QsS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDVixLQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztZQUN4QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNaLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNiLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2IsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDckUsaURBQWlEO1FBQ2pELDhDQUE4QztRQUM5QyxpQ0FBaUM7SUFDbkMsQ0FBQztJQUVELG9DQUFZLEdBQVosVUFBYSxZQUFvQjtRQUMvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDM0MsVUFBVSxFQUFFO2dCQUNSLElBQUksRUFBRSxNQUFNO2FBQ2Y7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QseUNBQWlCLEdBQWpCO1FBQ0ksSUFBTSxVQUFVLEdBQWtCLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNwRCxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQWxIUSxhQUFhO1FBUnpCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTTtZQUNoQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUd0QyxDQUFDO3lDQW1EOEIsZUFBTSxFQUE0Qix5QkFBZ0I7T0FsRHJFLGFBQWEsQ0FtSHpCO0lBQUQsb0JBQUM7Q0FBQSxBQW5IRCxJQW1IQztBQW5IWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCAqIGFzIGFwcCBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi9hcHBsaWNhdGlvblwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xuaW1wb3J0IHsgQ2Fyb3VzZWxEaXJlY3RpdmUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW5nMi1jYXJvdXNlbFwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFNlZ21lbnRlZEJhciwgU2VnbWVudGVkQmFySXRlbSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3NlZ21lbnRlZC1iYXIvc2VnbWVudGVkLWJhclwiO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeSc7XG5pbXBvcnQgeyBDYXJkVmlldyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1jYXJkdmlldyc7XG5yZWdpc3RlckVsZW1lbnQoJ0NhcmRWaWV3JywgKCkgPT4gQ2FyZFZpZXcpO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiSG9tZVwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCIuL2hvbWUuY29tcG9uZW50LmNzc1wiXSxcblxuXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGltYWdlczogQXJyYXk8YW55PiA9IFtdO1xuICAgIGltZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHB1YmxpYyBteUl0ZW1zOiBBcnJheTxTZWdtZW50ZWRCYXJJdGVtPjtcbiAgICBwdWJsaWMgcHJvcDogc3RyaW5nID0gXCJJdGVtIDFcIjtcbiAgICBwdWJsaWMgcm93cyA9W107XG4gICAgcHVibGljIGNvbHMgPVtdO1xuICAgIHB1YmxpYyBkYXRhZGlzcGxheSA9IFtdO1xuICAgIHB1YmxpYyByb3dzQ291bnQgPSAwO1xuICAgIHB1YmxpYyBmaW5hbGRhdGEgPSBbXTtcbiAgICBwdWJsaWMgZGF0YSA9IFtcbiAgICB7XCJuYW1lXCI6XCJTdWl0XCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL21hbm5cIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJKYWNrZXRzXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIkJsYWNrIHRzaGlydFwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdnXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiTWFuIEhvb2RpZVwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdlZVwiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdlZVwiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdlZVwiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICB7XCJuYW1lXCI6XCJcIixcIlByb2R1Y3RfbmFtZVwiOlwicmVzOi8vaW1nXCIsXCJQcm9kdWN0X2NvZGVcIjpcIjFcIn0sXG4gICAge1wibmFtZVwiOlwiXCIsXCJQcm9kdWN0X25hbWVcIjpcInJlczovL2ltZ1wiLFwiUHJvZHVjdF9jb2RlXCI6XCIxXCJ9LFxuICAgIHtcIm5hbWVcIjpcIlwiLFwiUHJvZHVjdF9uYW1lXCI6XCJyZXM6Ly9pbWdcIixcIlByb2R1Y3RfY29kZVwiOlwiMVwifSxcbiAgICAgICAgICAgICAgICAgICBdXG4gICAgcHVibGljIGFycnlyb3dzID0gXCIqLFwiO1xuICAgIHRybl90eXBlO1xuXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHtcbiAgICAgICAgdGhpcy5pbWFnZXMgPSBbXG4gICAgICAgICAgICB7IHRpdGxlOiAnU3Bvb3J0aHknLCB1cmw6ICdyZXM6Ly9zcCcgfSxcbiAgICAgICAgICAgIHsgdGl0bGU6ICdJbWFnZSAyJywgdXJsOiAncmVzOi8vc3BjJyB9LFxuICAgICAgICAgICAgeyB0aXRsZTogJ0ltYWdlIDMnLCB1cmw6ICdyZXM6Ly9zcGNtJyB9LFxuICAgICAgICBdO1xuXG4gICAgICAgIHRoaXMubXlJdGVtcyA9IFtdO1xuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8IDY7IGkrKykge1xuICAgICAgICAgICAgY29uc3QgaXRlbSA9IG5ldyBTZWdtZW50ZWRCYXJJdGVtKCk7XG4gICAgICAgICAgICBpdGVtLnRpdGxlID0gXCJUYWIgXCIgKyBpOyAgXG4gICAgICAgICAgICB0aGlzLm15SXRlbXMucHVzaChpdGVtKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICAvLyBJbml0IHlvdXIgY29tcG9uZW50IHByb3BlcnRpZXMgaGVyZS5cbiAgICAgICAgdGhpcy5zZXRTY3JlZW5kYXRhKClcbiAgICB9XG5cbiAgICBcblxuICAgIHNldFNjcmVlbmRhdGEoKXtcbiAgICAgICAgbGV0IHJvdyA9IDA7XG4gICAgICAgIGxldCBjb2x1bW4gPSAwO1xuICAgICAgICBsZXQgY291bnQgPSAwO1xuICAgICAgICB0aGlzLmRhdGEuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgICAgICB0aGlzLmRhdGFkaXNwbGF5LnB1c2goe1xuICAgICAgICAgICAgcm93Y291bnQ6IHJvdyxcbiAgICAgICAgICAgIGNvbHVtbmNvdW50OiBjb2x1bW4sXG4gICAgICAgICAgICBuYW1lOiBlbGVtZW50Lm5hbWUsXG4gICAgICAgICAgICBpbWd1cmw6ZWxlbWVudC5Qcm9kdWN0X25hbWUsXG4gICAgICAgICAgICBQcm9kdWN0Q29kZTplbGVtZW50LlByb2R1Y3RfY29kZVxuXG4gICAgICAgICAgfSlcbiAgICAgICAgICBpZiAoY291bnQgPT0gMSkge1xuICAgICAgICAgICAgcm93ID0gcm93ICsgMTtcbiAgICAgICAgICAgIGNvdW50ID0gMDtcbiAgICAgICAgICAgIHRoaXMuYXJyeXJvd3MgKz0gXCIqLFwiO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb3VudCA9IDE7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChjb2x1bW4gPT0gMCkge1xuICAgICAgICAgICAgY29sdW1uID0gMTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29sdW1uID0gMDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmFycnlyb3dzID0gdGhpcy5hcnJ5cm93cy5zdWJzdHJpbmcoMCwgdGhpcy5hcnJ5cm93cy5sZW5ndGggLSAxKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJEQVRBLURpc3BsYXlcIiwgdGhpcy5kYXRhZGlzcGxheSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwidGhpcy5hcnJ5cm93c1wiLCB0aGlzLmFycnlyb3dzKVxuICAgICAgICAvLyB0aGlzLmlzQXV0aGVudGljYXRpbmcgPSBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgb25OYXZJdGVtVGFwKG5hdkl0ZW1Sb3V0ZTogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbbmF2SXRlbVJvdXRlXSwge1xuICAgICAgICAgICAgdHJhbnNpdGlvbjoge1xuICAgICAgICAgICAgICAgIG5hbWU6IFwiZmFkZVwiXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBvbkRyYXdlckJ1dHRvblRhcCgpOiB2b2lkIHtcbiAgICAgICAgY29uc3Qgc2lkZURyYXdlciA9IDxSYWRTaWRlRHJhd2VyPmFwcC5nZXRSb290VmlldygpO1xuICAgICAgICBzaWRlRHJhd2VyLnNob3dEcmF3ZXIoKTtcbiAgICB9XG59XG4iXX0=