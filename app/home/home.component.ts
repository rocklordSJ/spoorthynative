import { Component, OnInit } from "@angular/core";
import * as app from "tns-core-modules/application/application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { CarouselDirective } from "nativescript-ng2-carousel";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationEnd, Router } from "@angular/router";
import { SegmentedBar, SegmentedBarItem } from "tns-core-modules/ui/segmented-bar/segmented-bar";
import { registerElement } from 'nativescript-angular/element-registry';
import { CardView } from 'nativescript-cardview';
registerElement('CardView', () => CardView);
@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],


})
export class HomeComponent implements OnInit {
    images: Array<any> = [];
    img: boolean = false;
    public myItems: Array<SegmentedBarItem>;
    public prop: string = "Item 1";
    public rows =[];
    public cols =[];
    public datadisplay = [];
    public rowsCount = 0;
    public finaldata = [];
    public data = [
    {"name":"Suit","Product_name":"res://mann","Product_code":"1"},
    {"name":"Jackets","Product_name":"res://img","Product_code":"1"},
    {"name":"Black tshirt","Product_name":"res://imgg","Product_code":"1"},
    {"name":"Man Hoodie","Product_name":"res://imgee","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://imgee","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://imgee","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
    {"name":"","Product_name":"res://img","Product_code":"1"},
                   ]
    public arryrows = "*,";
    trn_type;


    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        this.images = [
            { title: 'Spoorthy', url: 'res://sp' },
            { title: 'Image 2', url: 'res://spc' },
            { title: 'Image 3', url: 'res://spcm' },
        ];

        this.myItems = [];
        for (let i = 1; i < 6; i++) {
            const item = new SegmentedBarItem();
            item.title = "Tab " + i;  
            this.myItems.push(item);
        }
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.setScreendata()
    }

    

    setScreendata(){
        let row = 0;
        let column = 0;
        let count = 0;
        this.data.forEach(element => {
          this.datadisplay.push({
            rowcount: row,
            columncount: column,
            name: element.name,
            imgurl:element.Product_name,
            ProductCode:element.Product_code

          })
          if (count == 1) {
            row = row + 1;
            count = 0;
            this.arryrows += "*,";
          } else {
            count = 1;
          }
          if (column == 0) {
            column = 1;
          } else {
            column = 0;
          }
        });
        this.arryrows = this.arryrows.substring(0, this.arryrows.length - 1);
        // console.log("DATA-Display", this.datadisplay);
        // console.log("this.arryrows", this.arryrows)
        // this.isAuthenticating = false;
      }

      onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });
    }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
