"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var productdetails_routing_module_1 = require("~/home/homeproductdetails/productdetails-routing.module");
var productdetails_component_1 = require("./productdetails.component");
var ProductDetails = /** @class */ (function () {
    function ProductDetails() {
    }
    ProductDetails = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                productdetails_routing_module_1.HomeRoutingModule
            ],
            declarations: [
                productdetails_component_1.ProductDetailsComponent,
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], ProductDetails);
    return ProductDetails;
}());
exports.ProductDetails = ProductDetails;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdGRldGFpbHMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicHJvZHVjdGRldGFpbHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJEO0FBQzNELHNEQUF1RTtBQUN2RSx5R0FBNEY7QUFDNUYsdUVBQXFFO0FBY3JFO0lBQUE7SUFBOEIsQ0FBQztJQUFsQixjQUFjO1FBWjFCLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxpQ0FBd0I7Z0JBQ3hCLGlEQUFpQjthQUNwQjtZQUNELFlBQVksRUFBRTtnQkFDVixrREFBdUI7YUFDMUI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsdUJBQWdCO2FBQ25CO1NBQ0osQ0FBQztPQUNXLGNBQWMsQ0FBSTtJQUFELHFCQUFDO0NBQUEsQUFBL0IsSUFBK0I7QUFBbEIsd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQgeyBIb21lUm91dGluZ01vZHVsZSB9IGZyb20gXCJ+L2hvbWUvaG9tZXByb2R1Y3RkZXRhaWxzL3Byb2R1Y3RkZXRhaWxzLXJvdXRpbmcubW9kdWxlXCI7XG5pbXBvcnQgeyBQcm9kdWN0RGV0YWlsc0NvbXBvbmVudCB9IGZyb20gXCIuL3Byb2R1Y3RkZXRhaWxzLmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxuICAgICAgICBIb21lUm91dGluZ01vZHVsZVxuICAgIF0sXG4gICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIFByb2R1Y3REZXRhaWxzQ29tcG9uZW50LFxuICAgIF0sXG4gICAgc2NoZW1hczogW1xuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBQcm9kdWN0RGV0YWlscyB7IH1cbiJdfQ==