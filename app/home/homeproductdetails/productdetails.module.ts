import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { HomeRoutingModule } from "~/home/homeproductdetails/productdetails-routing.module";
import { ProductDetailsComponent } from "./productdetails.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        HomeRoutingModule
    ],
    declarations: [
        ProductDetailsComponent,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ProductDetails { }
